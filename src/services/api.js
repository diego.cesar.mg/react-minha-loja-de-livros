import axios from "axios";

export default axios.create({
  baseURL: `https://my-bookshop-srv-chipper-panda-ni.cfapps.eu10.hana.ondemand.com/catalog`
});
