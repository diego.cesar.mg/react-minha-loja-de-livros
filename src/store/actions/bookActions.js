import {
  GET_BOOKS,
  INSERT_CUSTOMER,
  DELETE_CUSTOMER,
  BOOKS_LOADING
} from "./types";
import api from "../../services/api";

export const getBooks = () => dispatch => {
  dispatch(setBooksLoading());

  return api
    .get("/Books")
    .then(res => {
      dispatch({
        type: GET_BOOKS,
        books: res.data.value
      });
    })
    .catch(error => {
      throw error;
    });
};

export const insertBook = book => dispatch => {
  return api
    .post("/Books", book)
    .then(res => {
      dispatch({
        type: INSERT_CUSTOMER,
        book: res.data.value
      });
    })
    .catch(error => {
      throw error;
    });
};

export const deleteBook = id => dispatch => {
  return api
    .delete(`/Books/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_CUSTOMER,
        _id: id
      });
    })
    .catch(error => {
      throw error;
    });
};

export const setBooksLoading = () => {
  return {
    type: BOOKS_LOADING
  };
};
