import {
  GET_BOOKS,
  INSERT_CUSTOMER,
  DELETE_CUSTOMER,
  BOOKS_LOADING
} from "../actions/types";

const initialState = {
  books: [],
  loading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_BOOKS:
      return {
        ...state,
        books: action.books,
        loading: false
      };
    case INSERT_CUSTOMER:
      return {
        ...state,
        books: [action.book, ...state.books]
      };
    case DELETE_CUSTOMER:
      return {
        ...state,
        books: state.books.filter(
          book => book._id !== action._id
        )
      };
    case BOOKS_LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
}
