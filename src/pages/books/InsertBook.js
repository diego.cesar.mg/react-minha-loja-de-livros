import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { connect } from "react-redux";
import { insertBook } from "../../store/actions/bookActions";

class InsertBook extends Component {
  state = {
    modal: false,
    dropdownOpen: false
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggleDropdown = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    // Dados do novo livro
    const newBook = {
      ID: this.state.ID,
      title: this.state.title,
    };

    // Adiciona o livro via action "addBook"
    this.props.insertBook(newBook);

    // Fecha a modal
    this.toggle();
  };

  render() {
    return (
      <div>
        <Button
          color='dark'
          style={{ marginBottom: "2rem", marginTop: "2rem" }}
          onClick={this.toggle}>
          Inserir livro
        </Button>
        <Modal isOpen={this.state.modal} autoFocus={false}>
          <ModalHeader toggle={this.toggle}>Novo livro</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <Label for='ID'>ID</Label>
                <Input
                  type='text'
                  id='ID'
                  name='ID'
                  placeholder='Adicionar um livro'
                  onChange={this.onChange}
                  autoFocus
                />
                <Label for='nameFantasy'>Título</Label>
                <Input
                  type='text'
                  id='title'
                  name='title'
                  onChange={this.onChange}
                />

                <Button color='dark' style={{ marginTop: "2rem" }} block>
                  Inserir
                </Button>
              </FormGroup>
            </Form>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  book: state.book
});

export default connect(
  mapStateToProps,
  { insertBook }
)(InsertBook);
