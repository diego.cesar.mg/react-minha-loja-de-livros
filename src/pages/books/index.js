import React, { Component } from "react";
import { Container, ListGroupItem, Table, Button } from "reactstrap";
import { connect } from "react-redux";
import { getBooks, deleteBook } from "../../store/actions/bookActions";
import PropTypes from "prop-types";

class BookList extends Component {
  componentDidMount() {
    this.props.getBooks();
  }

  onDeleteClick = id => {
    this.props.deleteBook(id);
  };

  render() {
    const { books } = this.props.book;
    return (
      <Container>
        <Table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Título</th>
            </tr>
          </thead>
          <tbody>
            {books.map(book => (
              <tr key={book._id}>
                {/* <th scope='row'>{book._id}</th> */}
                <th scope="row">{book.ID}</th>
                <th scope="row">{book.title}</th>
                <th scope="row">
                  <Button onClick={this.onDeleteClick.bind(this, book._id)} close />
                </th>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    );
  }
}

BookList.propTypes = {
  getBooks: PropTypes.func.isRequired,
  book: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  book: state.books
});

export default connect(
  mapStateToProps,
  { getBooks, deleteBook }
)(BookList);
