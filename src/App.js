import React from "react";
import AppNavBar from "./components/AppNavBar";
import BookList from "./pages/books";
import InsertBook from "./pages/books/InsertBook";
import { Container } from "reactstrap";

import { Provider } from "react-redux";
import store from "./store";

const App = () => (
  <Provider store={store}>
    <div className='App'>
      <AppNavBar />
      <Container>
        <InsertBook />
        <BookList />
      </Container>
    </div>
  </Provider>
);

export default App;
