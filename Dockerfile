#docker container prune && docker image prune && docker build -f Dockerfile -t seidordigital/my-bookshop-webapp . && docker run -p 5000:5000 --name=my-bookshop-webapp seidordigital/my-bookshop-webapp:latest
FROM node:8.16.0-alpine
# Copy the current directory contents into the container at /client
COPY . /client/
# Set the working directory to /client
WORKDIR /client
# install dependencies
RUN npm install && npm install -g serve && npm run build
# Run the app when the container launches
CMD ["serve", "-s", "build"]